$(function(){

	var classPage = $("body").attr("class");

	$(dropdownSelected);
	$(verifyInputTextEmpty);
	$(compareProperties);
	$(viewList);
	$(limitChar);
	$(carouselCompare);
	
	if (classPage == "page-home") {
		$(fullBanner);
		$(carouselSwipe);
		$(autoComplete);
	}

	if (classPage == "page-search"){
		$(toggleAdvancedAside);
		$(carouselThreeItems);
	} 

	if (classPage == "page-about") {
		$(carouselSwipe);
		$(anchorMenu);
		$(navFixedTop);
		$(openModalWithIndex);
	}

	if (classPage == "page-details") {
		$(carouselSwipe);
		$(toggleShortDescription);
	}
});

//FullBanner
function fullBanner(){
	$(".full-banner .carousel-inner .item").each(function(){
		var urlBanner = $(this).find("img").attr("src");
		$(this).css("background-image", "url(" + urlBanner + ")");
	});
}

//CarouselSwipe
function carouselSwipe(){
	$(".carousel").swipe({
		swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
			if (direction == 'left') $(this).carousel('next');
			if (direction == 'right') $(this).carousel('prev');
		},
		allowPageScroll:"vertical"
	});
}

//AutoComplete
function autoComplete(){
	$.get("assets/data/json_autocomplete.json", function(data){
		$(".typeahead").typeahead({ source:data });
	},'json');
}

//ToggleAside
function toggleAdvancedAside(){
	$(".box-aside .btn-show").click(function(e){
		e.preventDefault();

		var widthBoxItemSide = $(".box-item-aside").width();
		$(".box-item-aside .slider-horizontal").css("max-width", widthBoxItemSide - 35);
		$(".box-item-aside .slider-tick-label-container").css("margin-left", - widthBoxItemSide/9);
		$(".box-item-aside .slider-tick-label").css("width", widthBoxItemSide/5 + 7);

		$(".box-aside .box-item-aside").css("display", "block");
		$(".box-aside .btn-show").css("display", "none");
		$(".box-aside .btn-hide").css("display", "initial");
	});

	$(".box-aside .btn-hide").click(function(e){
		e.preventDefault();
		$(".box-aside .box-item-aside").css("display", "none");
		$(".box-aside .box-item-aside:eq(0)").css("display", "block");
		$(".box-aside .box-item-aside:eq(1)").css("display", "block");
		$(".box-aside .box-item-aside:eq(2)").css("display", "block");
		$(".box-aside .btn-show").css("display", "initial");
		$(".box-aside .btn-hide").css("display", "none");
	});	
}

//DropdownSelected
function dropdownSelected(){
	$(".dropdown-menu a").click(function(e){
		e.preventDefault();

		var caret = "<span class='caret'></span>";
		var button = $(this).parents(".dropdown").find("button");
		var valueSelected = $(this).attr("data-value");

		button.attr("value", valueSelected).html(valueSelected + caret).addClass("active");
		$(this).parent().siblings().find("a").removeClass("active");
		$(this).addClass("active");
		
	});
}

//VerifyInputText
function verifyInputTextEmpty(){
	$("input[type=text]").blur(function(){
		var field = $(this).val();
		if(!field == ""){
			$(this).addClass("active");
		} else {
			$(this).removeClass("active");
		}
	});
}

//ViewGridOrList
function viewList(){
	$(".view-grid-list > button").click(function(){
		var viewMode = $(this).attr("data-value");
		$(this).addClass("active").siblings().removeClass("active");

		if (viewMode == "list") {
			$(".box-list-properties").not(".box-list-compare").addClass("view-list");
		} else {
			$(".box-list-properties").removeClass("view-list");
		}
	});
}

//CompareProperties
function compareProperties(){

	var status = true;
	var count = 0;
	var caretAdded = "<i aria-hidden='true' class='fa fa-check'></i>";
    var caretCompare = "<i aria-hidden='true' class='fa fa-files-o'></i>";
    var targetDisabled;

	$(".compare a").click(function(e){
		e.preventDefault();

		if(!$(this).hasClass("active")){
			count += 1;
			$(this).addClass("active");
			$(this).html(caretAdded + " Added");
		} else {
			count -= 1;
			$(this).removeClass("active");
			$(this).html(caretCompare + " Compare");
		}

		if(count >= 2){
			$(".box-compare").fadeIn().find(".count").text(count);
		} else {
			$(".box-compare").fadeOut(100);
		}
		
	});	

	$(".box-compare .clearAll").click(function(e){
		e.preventDefault();

		$(".box-compare").fadeOut(100).find(".count").text("-")
		$(".box-list-properties .compare a").removeClass("active").html(caretCompare + " Compare");
		count = 0;
	});

	$(".box-compare .box-buttons .active").click(function(event) {
		event.preventDefault();
		targetDisabled = $(".owl-nav .disabled").index();
		$(".box-carousel-control > span").removeClass("disabled").eq(targetDisabled).addClass("disabled");	

		if (!$(".owl-nav").hasClass("disabled")) {
			$(".box-carousel-control").css("display", "block");			
		}

		if (status) {
			status = false;
			$(this).text("Hide");
			$("html, body").css("overflow", "hidden");
			$(".box-compare").addClass("open");
		} else {
			status = true;
			$(this).text("Show");
			$(".box-compare").removeClass("open");
			$("html, body").css("overflow", "auto");			
		}
	});

	$(".box-carousel-control > span").click(function(){
		
		var targetIndex = $(this).index();
		var targetContext = $(this).parents(".box-compare");
	
		$(".owl-nav div", targetContext).eq(targetIndex).trigger("click");

		setTimeout(function(){
			var targetDisabled = $(".owl-nav .disabled").index();	
			$(".box-carousel-control > span").removeClass("disabled").eq(targetDisabled).addClass("disabled");
		}, 200);
	});
}


//LimitChar
function limitChar() {
    $(".limit-char").each(function (i) {
        len = $(this).text().length;
        if (len >= 121) {
            $(this).text($(this).text().substr(0, 121) + '...');
        }
    });
}

//CarouselWithThreeItem
function carouselThreeItems(){
	$(".carousel-three-items .item").each(function(){
		var next = $(this).next();

		if (!next.length) {
			next = $(this).siblings(':first');
		}

		next.children(':first-child').clone().appendTo($(this));

		if (next.next().length>0) {
			next.next().children(':first-child').clone().appendTo($(this));
		} else {
			$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
		}
	});	
}

//NavFixed
function navFixedTop(){
	var posNav = $(".box-fixed-top").offset().top;
	
	$(window).scroll(function(){
		var posWindow = $(window).scrollTop();

		if(posWindow >= posNav){
			$(".box-fixed-top").addClass("fixed-active");
		} else {
			$(".box-fixed-top").removeClass("fixed-active");
		}
	});		
}

//OpenModalWithIndex
function openModalWithIndex(){
	$(".leader-list > li").click(function(){
		var indexLeader = $(this).index();
		$("#ModalLeader .leader-list > li").removeClass("active").eq(indexLeader).addClass("active");
	});
}

//MenuNavAbout
function anchorMenu(){
	$(".box-nav .navbar-nav a").click(function(){
		var anchor = $(this).attr("href");

	    $("html, body").animate({
	        scrollTop: $(anchor).offset().top - 50
	    }, 2000);		

		$(this).parent().siblings().removeClass("active");
		$(this).parent().addClass("active");
	});
}

//ToggleShortDescription
function toggleShortDescription(){
	var status = true;
	
	$(".box-description .box-toggle").click(function(){
		if (status) {
			status = false;
			$(this).text("Ver menos");
			$(".box-description").addClass("active");
		} else {
			status = true;
			$(this).text("Ver mais");
			$(".box-description").removeClass("active");			
		}
	});
}

function carouselCompare(){
	$(".owl-carousel").owlCarousel({
		loop:true,
		margin:10,
		responsiveClass: true,
		responsive:{
			0:{
				items: 1,
				nav: true
			},			        
			600:{
				items: 2,
				nav: false
			},
			1000:{
				items: 3,
				nav: true,
				loop: false
			}
		}
	});
}

